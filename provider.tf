# We strongly recommend using the required_providers block to set the
# Azure Provider source and version being used
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}

# Create a resource group
#resource "azurerm_resource_group" "paco-garcia_rg" {
#  name     = "paco-garcia_rg"
#  location = "West Europe"
#}

# Create a virtual network within the resource group
#resource "azurerm_virtual_network" "paco-garcia_vn" {
# name                = "paco-garcia_vn"
#  resource_group_name = azurerm_resource_group.example.name
# location            = azurerm_resource_group.example.location
#  address_space       = ["10.0.0.0/16"]
#}
