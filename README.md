# Asrbd Terraform Eval On Azure

## Information de connexion :

```bash
az login
--
terraform init
--
terraform apply

Value : westus
Value : pacogarcia
--
ssh adminuser@[Voir IP dans Email]
ssh-copy-id adminuser@IP
---
```

# ANSIBLE :

```bash
ansible -i hosts azure -m ping  

```

### Ansible Playbook :

```bash
ansible-playbook -i hosts  azure.yml    

```

### Ansible Adhoc :

```bash
ansible azure  -m ansible.builtin.apt -a "name=nginx state=present" 
ansible -i azure  -m ansible.builtin.apt -a "name=nginx state=present"      

```
